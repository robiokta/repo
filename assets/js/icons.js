var icons = [
    {
      "id": "add"
    },
    {
      "id": "add-circle"
    },
    {
      "id": "add-circle-outline"
    },
    {
      "id": "airplane"
    },
    {
      "id": "alarm"
    },
    {
      "id": "albums"
    },
    {
      "id": "alert"
    },
    {
      "id": "american-football"
    },
    {
      "id": "analytics"
    },
    {
      "id": "aperture"
    },
    {
      "id": "apps"
    },
    {
      "id": "appstore"
    },
    {
      "id": "archive"
    },
    {
      "id": "arrow-back"
    },
    {
      "id": "arrow-down"
    },
    {
      "id": "arrow-dropdown"
    },
    {
      "id": "arrow-dropdown-circle"
    },
    {
      "id": "arrow-dropleft"
    },
    {
      "id": "arrow-dropleft-circle"
    },
    {
      "id": "arrow-dropright"
    },
    {
      "id": "arrow-dropright-circle"
    },
    {
      "id": "arrow-dropup"
    },
    {
      "id": "arrow-dropup-circle"
    },
    {
      "id": "arrow-forward"
    },
    {
      "id": "arrow-round-back"
    },
    {
      "id": "arrow-round-down"
    },
    {
      "id": "arrow-round-forward"
    },
    {
      "id": "arrow-round-up"
    },
    {
      "id": "arrow-up"
    },
    {
      "id": "at"
    },
    {
      "id": "attach"
    },
    {
      "id": "backspace"
    },
    {
      "id": "barcode"
    },
    {
      "id": "baseball"
    },
    {
      "id": "basket"
    },
    {
      "id": "basketball"
    },
    {
      "id": "battery-charging"
    },
    {
      "id": "battery-dead"
    },
    {
      "id": "battery-full"
    },
    {
      "id": "beaker"
    },
    {
      "id": "bed"
    },
    {
      "id": "beer"
    },
    {
      "id": "bicycle"
    },
    {
      "id": "bluetooth"
    },
    {
      "id": "boat"
    },
    {
      "id": "body"
    },
    {
      "id": "bonfire"
    },
    {
      "id": "book"
    },
    {
      "id": "bookmark"
    },
    {
      "id": "bookmarks"
    },
    {
      "id": "bowtie"
    },
    {
      "id": "briefcase"
    },
    {
      "id": "browsers"
    },
    {
      "id": "brush"
    },
    {
      "id": "bug"
    },
    {
      "id": "build"
    },
    {
      "id": "bulb"
    },
    {
      "id": "bus"
    },
    {
      "id": "business"
    },
    {
      "id": "cafe"
    },
    {
      "id": "calculator"
    },
    {
      "id": "calendar"
    },
    {
      "id": "call"
    },
    {
      "id": "camera"
    },
    {
      "id": "car"
    },
    {
      "id": "card"
    },
    {
      "id": "cart"
    },
    {
      "id": "cash"
    },
    {
      "id": "cellular"
    },
    {
      "id": "chatboxes"
    },
    {
      "id": "chatbubbles"
    },
    {
      "id": "checkbox"
    },
    {
      "id": "checkbox-outline"
    },
    {
      "id": "checkmark"
    },
    {
      "id": "checkmark-circle"
    },
    {
      "id": "checkmark-circle-outline"
    },
    {
      "id": "clipboard"
    },
    {
      "id": "clock"
    },
    {
      "id": "close"
    },
    {
      "id": "close-circle"
    },
    {
      "id": "close-circle-outline"
    },
    {
      "id": "cloud"
    },
    {
      "id": "cloud-circle"
    },
    {
      "id": "cloud-done"
    },
    {
      "id": "cloud-download"
    },
    {
      "id": "cloud-outline"
    },
    {
      "id": "cloud-upload"
    },
    {
      "id": "cloudy"
    },
    {
      "id": "cloudy-night"
    },
    {
      "id": "code"
    },
    {
      "id": "code-download"
    },
    {
      "id": "code-working"
    },
    {
      "id": "cog"
    },
    {
      "id": "color-fill"
    },
    {
      "id": "color-filter"
    },
    {
      "id": "color-palette"
    },
    {
      "id": "color-wand"
    },
    {
      "id": "compass"
    },
    {
      "id": "construct"
    },
    {
      "id": "contact"
    },
    {
      "id": "contacts"
    },
    {
      "id": "contract"
    },
    {
      "id": "contrast"
    },
    {
      "id": "copy"
    },
    {
      "id": "create"
    },
    {
      "id": "crop"
    },
    {
      "id": "cube"
    },
    {
      "id": "cut"
    },
    {
      "id": "desktop"
    },
    {
      "id": "disc"
    },
    {

      "id": "document"
    },
    {
      "id": "done-all"
    },
    {
      "id": "download"
    },
    {
      "id": "easel"
    },
    {
      "id": "egg"
    },
    {
      "id": "exit"
    },
    {
      "id": "expand"
    },
    {
      "id": "eye"
    },
    {
      "id": "eye-off"
    },
    {
      "id": "fastforward"
    },
    {
      "id": "female"
    },
    {
      "id": "filing"
    },
    {
      "id": "film"
    },
    {
      "id": "finger-print"
    },
    {
      "id": "fitness"
    },
    {
      "id": "flag"
    },
    {
      "id": "flame"
    },
    {
      "id": "flash"
    },
    {
      "id": "flash-off"
    },
    {
      "id": "flashlight"
    },
    {
      "id": "flask"
    },
    {
      "id": "flower"
    },
    {
      "id": "folder"
    },
    {
      "id": "folder-open"
    },
    {
      "id": "football"
    },
    {
      "id": "funnel"
    },
    {
      "id": "gift"
    },
    {
      "id": "git-branch"
    },
    {
      "id": "git-commit"
    },
    {
      "id": "git-compare"
    },
    {
      "id": "git-merge"
    },
    {
      "id": "git-network"
    },
    {
      "id": "git-pull-request"
    },
    {
      "id": "glasses"
    },
    {
      "id": "globe"
    },
    {
      "id": "grid"
    },
    {
      "id": "hammer"
    },
    {
      "id": "hand"
    },
    {
      "id": "happy"
    },
    {
      "id": "headset"
    },
    {
      "id": "heart"
    },
    {
      "id": "heart-dislike"
    },
    {
      "id": "heart-empty"
    },
    {
      "id": "heart-half"
    },
    {
      "id": "help"
    },
    {
      "id": "help-buoy"
    },
    {
      "id": "help-circle"
    },
    {
      "id": "help-circle-outline"
    },
    {
      "id": "home"
    },
    {
      "id": "hourglass"
    },
    {
      "id": "ice-cream"
    },
    {
      "id": "image"
    },
    {
      "id": "images"
    },
    {
      "id": "infinite"
    },
    {
      "id": "information"
    },
    {
      "id": "information-circle"
    },
    {
      "id": "information-circle-outline"
    },
    {
      "id": "jet"
    },
    {
      "id": "journal"
    },
    {
      "id": "key"
    },
    {
      "id": "keypad"
    },
    {
      "id": "laptop"
    },
    {
      "id": "leaf"
    },
    {
      "id": "link"
    },
    {
      "id": "list"
    },
    {
      "id": "list-box"
    },
    {
      "id": "locate"
    },
    {
      "id": "lock"
    },
    {
      "id": "log-in"
    },
    {
      "id": "log-out"
    },
    {
      "id": "magnet"
    },
    {
      "id": "mail"
    },
    {
      "id": "mail-open"
    },
    {
      "id": "mail-unread"
    },
    {
      "id": "male"
    },
    {
      "id": "man"
    },
    {
      "id": "map"
    },
    {
      "id": "medal"
    },
    {
      "id": "medical"
    },
    {
      "id": "medkit"
    },
    {
      "id": "megaphone"
    },
    {
      "id": "menu"
    },
    {
      "id": "mic"
    },
    {
      "id": "mic-off"
    },
    {
      "id": "microphone"
    },
    {
      "id": "moon"
    },
    {
      "id": "more"
    },
    {
      "id": "move"
    },
    {
      "id": "musical-note"
    },
    {
      "id": "musical-notes"
    },
    {
      "id": "navigate"
    },
    {
      "id": "notifications"
    },
    {
      "id": "notifications-off"
    },
    {
      "id": "notifications-outline"
    },
    {
      "id": "nuclear"
    },
    {
      "id": "nutrition"
    },
    {
      "id": "open"
    },
    {
      "id": "options"
    },
    {
      "id": "outlet"
    },
    {
      "id": "paper"
    },
    {
      "id": "paper-plane"
    },
    {
      "id": "partly-sunny"
    },
    {
      "id": "pause"
    },
    {
      "id": "paw"
    },
    {
      "id": "people"
    },
    {
      "id": "person"
    },
    {
      "id": "person-add"
    },
    {
      "id": "phone-landscape"
    },
    {
      "id": "phone-portrait"
    },
    {
      "id": "photos"
    },
    {
      "id": "pie"
    },
    {
      "id": "pin"
    },
    {
      "id": "pint"
    },
    {
      "id": "pizza"
    },
    {
      "id": "planet"
    },
    {

      "id": "play"
    },
    {
      "id": "play-circle"
    },
    {
      "id": "podium"
    },
    {
      "id": "power"
    },
    {
      "id": "pricetag"
    },
    {
      "id": "pricetags"
    },
    {
      "id": "print"
    },
    {
      "id": "pulse"
    },
    {
      "id": "qr-scanner"
    },
    {
      "id": "quote"
    },
    {
      "id": "radio"
    },
    {
      "id": "radio-button-off"
    },
    {
      "id": "radio-button-on"
    },
    {
      "id": "rainy"
    },
    {
      "id": "recording"
    },
    {
      "id": "redo"
    },
    {
      "id": "refresh"
    },
    {
      "id": "refresh-circle"
    },
    {
      "id": "remove"
    },
    {
      "id": "remove-circle"
    },
    {
      "id": "remove-circle-outline"
    },
    {
      "id": "reorder"
    },
    {
      "id": "repeat"
    },
    {
      "id": "resize"
    },
    {
      "id": "restaurant"
    },
    {
      "id": "return-left"
    },
    {
      "id": "return-right"
    },
    {
      "id": "reverse-camera"
    },
    {
      "id": "rewind"
    },
    {
      "id": "ribbon"
    },
    {
      "id": "rocket"
    },
    {
      "id": "rose"
    },
    {
      "id": "sad"
    },
    {
      "id": "save"
    },
    {
      "id": "school"
    },
    {
      "id": "search"
    },
    {
      "id": "send"
    },
    {
      "id": "settings"
    },
    {
      "id": "share"
    },
    {
      "id": "share-alt"
    },
    {
      "id": "shirt"
    },
    {
      "id": "shuffle"
    },
    {
      "id": "skip-backward"
    },
    {
      "id": "skip-forward"
    },
    {
      "id": "snow"
    },
    {
      "id": "speedometer"
    },
    {
      "id": "square"
    },
    {
      "id": "square-outline"
    },
    {
      "id": "star"
    },
    {
      "id": "star-half"
    },
    {
      "id": "star-outline"
    },
    {
      "id": "stats"
    },
    {
      "id": "stopwatch"
    },
    {
      "id": "subway"
    },
    {
      "id": "sunny"
    },
    {
      "id": "swap"
    },
    {
      "id": "switch"
    },
    {
      "id": "sync"
    },
    {
      "id": "tablet-landscape"
    },
    {
      "id": "tablet-portrait"
    },
    {
      "id": "tennisball"
    },
    {
      "id": "text"
    },
    {
      "id": "thermometer"
    },
    {
      "id": "thumbs-down"
    },
    {
      "id": "thumbs-up"
    },
    {
      "id": "thunderstorm"
    },
    {
      "id": "time"
    },
    {
      "id": "timer"
    },
    {
      "id": "today"
    },
    {
      "id": "train"
    },
    {
      "id": "transgender"
    },
    {
      "id": "trash"
    },
    {
      "id": "trending-down"
    },
    {
      "id": "trending-up"
    },
    {
      "id": "trophy"
    },
    {
      "id": "tv"
    },
    {
      "id": "umbrella"
    },
    {
      "id": "undo"
    },
    {
      "id": "unlock"
    },
    {
      "id": "videocam"
    },
    {
      "id": "volume-high"
    },
    {
      "id": "volume-low"
    },
    {
      "id": "volume-mute"
    },
    {
      "id": "volume-off"
    },
    {
      "id": "walk"
    },
    {
      "id": "wallet"
    },
    {
      "id": "warning"
    },
    {
      "id": "watch"
    },
    {
      "id": "water"
    },
    {
      "id": "wifi"
    },
    {
      "id": "wine"
    },
    {
      "id": "woman"
    }
  ];