<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$this->load->view('layout/header');
?>
<?php echo validation_errors(); ?>
<?php echo @$error; ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<h1 class="h2"><?= empty($menu_data->id)?'Add':'Edit' ?> Pegawai</h1>
</div>
<form method="post" action="<?=base_url('pegawai/save')?>" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?=$menu_data->id ?? ''?>">
	<div class="row my-3">
		<div class="col-md-6">
			<div class="row mb-3">
				<div class="col-4">
					<h5>Nama</h5>
				</div>
				<div class="col">
					<input type="text" class="form-control" value="<?=$menu_data->nama ?? ''?>" name="nama" >
				</div>
			</div>
			<div class="row my-3">
				<div class="col-4">
					<h5>Tinggi</h5>
				</div>
				<div class="col">
					<input type="text" class="form-control"value="<?=$menu_data->tinggi ?? ''?>" name="tinggi">
				</div>
			</div>
			<div class="row my-3">
				<div class="col-4">
					<h5>Tanggal Lahir</h5>
				</div>
				<div class="col">
					<input type="date" name="tgl_lahir" value="<?=$menu_data->tgl_lahir ?? ''?>">
				</div>
			</div>
			<div class="row my-3">
				<div class="col">
					<input type="file" id="avatar" accept="image/png, image/jpeg" name="img">
				</div>
			</div>
			<div class="row my-3">
				<div class="col">
					<button type="submit" class="btn btn-primary btn-block">save</button>
				</div>
				<div class="col">
					<a href="" type="button" class="btn btn-light btn-block">back</a>
				</div>

			</div>    
		</div>

	</div>
</form>

<?php $this->load->view('layout/footer'); ?>