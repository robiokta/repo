<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('layout/header');
?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
    <h1 class="h2">Jabatan</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="<?=base_url('jabatan/add')?>" class="btn btn-sm btn-success"><span data-feather="plus"></span>New Data</a>
            <button class="btn btn-sm btn-info">Export</button>
        </div>
    </div>
</div>

<table class="table">
  <tr>
    <th>id</th>
    <th>id_jabatan</th>
    <th>Jabatan</th>
    <th>Nama</th>   
    <th>Tinggi</th>    
    <th>Tanggal Lahir</th>    
    <th>Foto</th>    
  </tr>
  <?php 
 
  foreach ($jabatan as $row) { ?>
  <tr>
  
  <td><?php echo $row->id;?></td>
  <td><?php echo $row->fk_pegawai;?></td>
  <td><?php echo $row->jabtan?></td>
  <td><?php echo $row->nama;?></td>
  <td><?php echo $row->tinggi;?></td>
  <td><?php echo $row->tgl_lahir;?></td>
  <td><img src="<?php echo $row->img;?>" alt="Foto" width="42" height="42"></td>
  </tr>
    <?php } ?>
    
</table>

<?php $this->load->view('layout/footer'); ?>
