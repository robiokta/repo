<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$this->load->view('layout/header');
?>
<style>
.dataTables_filter{
	    display: inline;
    float: right;
}
</style>
<script>
	$(function(){
		$('#myTable').DataTable({
			dom: 'Bfrtip',
			buttons: [
			{ 
				extend: 'excel',
				className: 'btn-info',
				exportOptions: {
					columns: ':visible'
				}
			},{ 
				extend: 'pdf',
				className: 'btn-info',
				exportOptions: {
					columns: ':visible'
				}
			},
			{ 
				extend: 'print',
				className: 'btn-info',
				exportOptions: {
					columns: ':visible'
				}
			},
			{
				extend: 'colvis',
				className: 'btn-info',
				text: "Pilih Kolom",
				postfixButtons: [ 'colvisRestore' ]
			}
			],
			columnDefs: [ {
				targets: -1,
				visible: true
			} ],

			"language": {
				lengthMenu: "Display _MENU_ records per page",
				zeroRecords: "Nothing found - sorry",
				info: "Menampilkan _PAGE_ dari _PAGES_",
				infoEmpty: "Data Tidak ditemukan",
				infoFiltered: "(filtered from _MAX_ total records)",

			},

			paging:   true,
			ordering: true,
			processing: true,
			serverSide: true,
			order:[],

			ajax: { 
				type: "POST",
				url: "<?php echo base_url('pegawai/data'); ?>",

			},
			
			columns: [
			{
				"data": "id"

			},
			{ data: 'nama', name: 'nama' },
			{ data: 'tinggi', name: 'tinggi' },
			{ data: 'tgl_lahir', name: 'tgl_lahir' },
			{ data: 'jabtan', name: 'jabtan' },
			{
				"data": "img",
				render: function (data, type, row, meta) {
					return `<img src="${data}" alt="Foto" width="42" height="42">
					`;
				}
			},
			{
				"data": "option",
				render: function (data, type, row, meta) {
					return `<a href="<?=base_url('pegawai/edit-') ?>${data}" class="btn btn-sm btn-info text-white">
					<span data-feather="edit"></span></a>
					<a href="<?=base_url('pegawai/delete/') ?>${data}" onClick="return confirm('Are You Sure to Delete this data #${data}?')"  class="btn btn-sm btn-danger text-white">
						<span data-feather="trash-2"></span></a>`;
					}
				},
				],

				drawCallback: function(setting){
					feather.replace();
				}
			});
});

</script>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<h1 class="h2">Pegawai</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="btn-group mr-2">
			<a href="<?=base_url('pegawai/add')?>" class="btn btn-sm btn-success"><span data-feather="plus"></span>New Data</a>
			<button class="btn btn-sm btn-info">Export</button>
		</div>
	</div>
</div>
<table id="myTable" class="table table-striped">
	<thead>
		<tr>
			<TH>#</TH>
			<TH>Nama</TH>
			<TH>Tinggi</TH>
			<TH>Tanggal Lahir</TH>
			<TH>Jabatan</TH>
			<TH>Foto</TH>
			<TH>Option</TH>
		</tr>

		<tbody>
		</tbody>
	</thead>

</table>

