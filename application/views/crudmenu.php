<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$this->load->view('layout/header');
?>
<style>
	.select2-selection{
		height: 80px !important;
	} 

</style>
<!-- membuat icon load dari javascript-->
<script>
	var select2_icons = $.map(icons,function(icon){
		
		icon.text= `<div><div class="w-25"><i class="icon ion-md-${icon.id} h3" ></i></div>${icon.id}</div>`
		return icon;
	});
	$(function(){
		$("#icon").select2({
			data:select2_icons,
			templateResult:function(data){
				return $(data.text);
			},templateSelection:function (data) {
				return $(data.text);
			},
		});
		$("#icon").val('<?= $menu_data->icon?? 'disc' ?>').trigger('change');

	});
</script>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<h1 class="h2"><?= empty($menu_data->id)?'Add':'Edit' ?> Menu</h1>
</div>
<form method="post" action="<?=base_url('menu/save')?>">
	<input type="hidden" name="id" value="<?=$menu_data->id ?? ''?>">
	<div class="row my-3">
		<div class="col-md-6">
			<div class="row mb-3">
				<div class="col-4">
					<h5>Title</h5>
				</div>
				<div class="col">
				<input type="text" class="form-control" value="<?=$menu_data->tittle ?? '' ?>" name="tittle" >
				</div>
			</div>
			<div class="row my-3">
				<div class="col-4">
					<h5>Slug</h5>
				</div>
				<div class="col">
					<input type="text" class="form-control"value="<?=$menu_data->slug ?? '' ?>" name="slug">
				</div>
			</div>
			<div class="row my-3">
				<div class="col-4">
					<h5>Icon</h5>
				</div>
				<div class="col">
					<select name="icon" id="icon" class="form-control"></select>
				</div>
			</div>
			<div class="row my-3">
				<div class="col">
					<button type="submit" class="btn btn-primary btn-block">save</button>
				</div>
				<div class="col">
					<a href="<?=base_url('menu')?>" type="button" class="btn btn-light btn-block">back</a>
				</div>

			</div>    
		</div>

	</div>
</form>

<?php $this->load->view('layout/footer'); ?>