<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$this->load->view('layout/header');
?>

<script>

$(function(){
	$('#myTable').DataTable({
		"language": { 
            lengthMenu: "Display _MENU_ records per page",
            zeroRecords: "Nothing found - sorry",
            info: "Menampilkan _PAGE_ dari _PAGES_",
            infoEmpty: "Data Tidak ditemukan",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
		paging:   false,
        ordering: true,
		processing: true,
		serverSide: true,
		order:[],
		ajax: {
			type: "POST",
			url: "<?php echo base_url('menu/data'); ?>",

		},
		columnDefs: [
        { targets: [0], searchable: false},
        { targets: [1], searchable: false},
        { targets: [2], searchable: false},
        { targets: [4], visible:true}
        // { targets: '_all', visible: false}

      ],
		columns: [
			{
				"data": "id"
				
			},
			{ data: 'tittle', name: 'tittle' },
			{ data: 'slug', name: 'slug' },
			{ data: 'icon', name: 'icon' },
			{
				"data": "option",
				render: function (data, type, row, meta) {
					return `<a href="<?=base_url('menu/edit/') ?>${data}" class="btn btn-sm btn-info text-white">
					<span data-feather="edit"></span></a>
					<a href="<?=base_url('menu/delete/') ?>${data}" onClick="return confirm('Are You Sure to Delete this data #${data}?')"  class="btn btn-sm btn-danger text-white">
					<span data-feather="trash-2"></span></a>`;
				}
			},
		],
		drawCallback: function(setting){
			feather.replace();
		}
	});
});

</script>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<h1 class="h2">Menu</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="btn-group mr-2">
			<a href="<?=base_url('menu/add')?>" class="btn btn-sm btn-success"><span data-feather="plus"></span>New Data</a>
			<button class="btn btn-sm btn-info">Export</button>
		</div>
	</div>
</div>
<table id="myTable" class="table table-striped">
	<thead>
		<tr>
			<TH>#</TH>
			<TH>Name</TH>
			<TH>Slug</TH>
			<TH>Icon</TH>
			<TH>Option</TH>
		</tr>

		<tbody>
		</tbody>
		</thead>

	</table>


	<?php $this->load->view('layout/footer'); ?>