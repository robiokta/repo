<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$this->load->view('layout/header');
?>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<h1 class="h2"><?= empty($menu_data->id)?'Add':'Edit' ?> Jabatan</h1>
</div>
<form method="post" action="<?=base_url('jabatan/save')?>" >
	<div class="row my-3">
		<div class="col-md-6">
			<div class="row mb-3">
				<div class="col-4">
					<h5>Jabatan</h5>
				</div>
				<div class="col">
					<select name="jabatan">
						<?php foreach($list as $li){ ?>
						<option value="<?=$li->nama_jabatan ?>"><?= $li->nama_jabatan ?>	
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="row my-3">
			<div class="col-4">
				<h5>Nama Pegawai</h5>
			</div>
			<div class="col">
				<select name="nama">
						<?php foreach($listnama as $lis){ ?>
						<option value="<?=$lis->id ?>"><?= $lis->nama ?>	
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="row my-3">
			<div class="col">
				<button type="submit" class="btn btn-primary btn-block">save</button>
			</div>
			<div class="col">
				<a href="" type="button" class="btn btn-light btn-block">back</a>
			</div>

		</div>    
	</div>

</div>
</form>

<?php $this->load->view('layout/footer'); ?>