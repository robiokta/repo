<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Jabatan_model extends CI_Model{
	
	public function jabatan(){
		$this->db->select('*');
		$this->db->from('jabatan');
		$this->db->join('pegawai','pegawai.id=jabatan.fk_pegawai');
		$query = $this->db->get();
		return $query->result();
	}
	public function list(){
		$hasil=$this->db->query("SELECT * FROM list_jabatan");

		return $hasil->result();

	}
	public function listnama(){
		$hasil=$this->db->query("SELECT * FROM pegawai");

		return $hasil->result();

	}
	public function insert($jabatan,$id){
		$query=$this->db->insert('jabatan',array('jabtan'=>$jabatan,'fk_pegawai'=>$id));
		
	}
}