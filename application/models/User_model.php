 <?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_Model{
	public $tittle;
	public $slug;
	public function get_last_ten_menus(){
		$query=$this->db->get('menu',10);
		return $query->result();
	}
	public function get_menu($id){
		$query=$this->db->get_where('menu',array('id'=>$id));
		return $query->result(); 
		
	}
	public function save($data){
		$this->db->where('id',$data['id']);
		$q = $this->db->get('menu');
		//jika ada maka akan di update
		if ($q->num_rows()>0) {
			$this->db->where('id',$data['id']);
			$this->db->update('menu',$data);
			# code...
		}else{ //jika tidak ada maka akan di insert .
			$this->db->set('id',$data['id']);
			$this->db->insert('menu',$data);
		}

	}
	public function delete($id){
		$this->db->where('id',$id);
		$this->db->delete('menu');

	}
	public function chart()
	{
		# code...
	}
}