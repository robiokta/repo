 <?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Pegawai_model extends CI_Model{
	public function pegawai($data){

		//$this->db->set('id',$data['id']);
		$this->db->insert('pegawai',$data);
		
	}
	public function get_pegawai($id){
		$query=$this->db->get_where('pegawai',array('id'=>$id));
		return $query->result();
	}  
	public function save($data){
		$this->db->where('id',$data['id']);
		$q = $this->db->get('pegawai');
		//jika ada maka akan di update
		if ($q->num_rows()>0) {
			$this->db->where('id',$data['id']);
			$this->db->update('pegawai',$data);

			# code...
		}else{ //jika tidak ada maka akan di insert .
			$this->db->set('id',$data['id']);
			$this->db->insert('pegawai',$data);
		}

	}
	public function data(){
		$this->datatables->select('`pegawai.id`, nama, tinggi, tgl_lahir,jabtan,img, id as option');
		$this->datatables->from('pegawai');
		$this->datatables->join('jabatan', '`pegawai`.`id` = `jabatan`.`fk_pegawai`');
		

		return $this->datatables->generate();
	}
}