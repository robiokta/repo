<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Jabatan extends CI_Controller{
	function __construct(){
		parent::__construct(); 

		$this->load->model('user_model');
		$this->load->model('jabatan_model');
		$this->load->helper('login_helper');
		cekLogin();
		$this->load->library('datatables');
		
	}
	public function index(){
		
		$data['menu']=$this->user_model->get_last_ten_menus();
		// $this->load->view('jabatan',array('menu'=>$data
		// 	));
		$data['jabatan'] = $this->jabatan_model->jabatan();
		$this->load->view('jabatan',$data);
	}
	public function add(){
		$data['menu']=$this->user_model->get_last_ten_menus();
		$data['list'] = $this->jabatan_model->list();
		$data['listnama'] = $this->jabatan_model->listnama();
		$this->load->view('crudjabatan',$data);
	}
	public function save(){
		$jabatan=$this->input->post('jabatan',TRUE);
		$id=$this->input->post('nama',TRUE);
		$register= $this->jabatan_model->insert($jabatan,$id);
		header("Location: ".base_url('jabatan'));
	}
}
