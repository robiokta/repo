<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('datatables');
		$this->load->Library('session');
        //$this->load->model('pegawai_model');
		$this->load->library('form_validation');
		if (empty($this->session->login)) {
			header("Location: ".base_url('login'));
		}
	}


	public function index()

	{
		if (empty($this->session->login)) {
			header("Location: ".base_url('login'));
			# code...
		}
		$data=$this->user_model->get_last_ten_menus();
		$this->load->view('notfound');
		
		
	}
}