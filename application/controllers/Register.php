<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Register_model');
		if (empty($this->session->login)) {
			header("Location: ".base_url('login'));
			# code...
		}
	}
	public function index()
	{
		
		$this->load->view('register');		
	}
	public function register(){
		$email=$this->input->post('email',TRUE);
		$password=$this->input->post('password',TRUE);
		$register= $this->Register_model->insert($email,$password);
	}
}