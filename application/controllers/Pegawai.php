 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('datatables');
		$this->load->Library('session');
        //$this->load->model('pegawai_model');
		$this->load->library('form_validation');
		if (empty($this->session->login)) {
			header("Location: ".base_url('login'));
		}
	}


	public function index()

	{
		if (empty($this->session->login)) {
			header("Location: ".base_url('login'));
			# code...
		}
		$this->load->helper('date');
		
		$data=$this->user_model->get_last_ten_menus();
		$this->load->view('pegawai',array('menu'=>$data
			));
		
		
	}
	public function data(){
		echo $this->pegawai_model->data();
		
	} 

	public function edit($id){
		$data=$this->user_model->get_last_ten_menus();
		$menu_data=$this->pegawai_model->get_pegawai($id);
		
		if (count($menu_data)>0) {
			$this->load->view('crudpegawai',array('menu'=>$data,
				'menu_data'=>$menu_data[0]
				));

	# code...
		}else{
			header("Location: ".base_url('menu'));
		}
		
	}
	public function save(){
		$data = $this->input->post();
		$this->pegawai_model->save($data); 
		//header("Location: ".base_url('pegawai'));
	}
	public function add(){
		$data=$this->user_model->get_last_ten_menus();
		$this->load->view('crudpegawai',array('menu'=>$data
			));
		
	}
	public function add_pegawai(){
		
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('tinggi', 'Tinggi', 'required');
		if ($this->form_validation->run() == FALSE)
		{

			$data=$this->user_model->get_last_ten_menus();
			$this->load->view('crudpegawai',array('menu'=>$data
				));
		}
		else
		{
			$nama=$this->input->post('nama');
			$tinggi=$this->input->post('tinggi');
			$tgl=$this->input->post('tgl_lahir');
			

		//upload photo
			//$config['max_size']=2048;
			$config['allowed_types']="png|jpg|jpeg|gif";
			$config['remove_spaces']=TRUE;
			$config['overwrite']=TRUE;
			$config['upload_path']=FCPATH.'gambar';

			$this->load->library('upload');
			$this->upload->initialize($config);

// print_r($_FILES);
		//ambil data image
			if ( ! $this->upload->do_upload('img'))
			{
				// $error = array('error' => $this->upload->display_errors());

				//$this->load->view('pegawai', $error);
				$data=$this->user_model->get_last_ten_menus();
				$this->load->view('pegawai',array('menu'=>$data,
					'error' => $this->upload->display_errors()));
			}
			else
			{
				$data_image=$this->upload->data('file_name');
				$location=base_url().'gambar/';
				$pict=$location.$data_image;


				$data=array(
					'nama'=>$nama,
					'tinggi'=>$tinggi,
					'tgl_lahir'=>$tgl,
					'img'=> $pict
					);
		//simpan data 
				$this->pegawai_model->pegawai($data);
				header("location: ".base_url('pegawai'));

			}
			
			
		}

	}
	
}