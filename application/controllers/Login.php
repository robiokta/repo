<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Login_model');
	}
	public function index()
	{
		
		$this->load->view('login');		
	}

	
	public function logmein(){
		$email=$this->input->post('email',TRUE);
		$password=$this->input->post('password',TRUE);
		$this->Login_model->validate_login($email,$password);
		
		$login=$this->Login_model->validate_login($email,$password);
		if (!empty($login)) {
			$this->session->login=$login;
			header("Location: ".base_url('admin/welcome'));
			# code...
		}else {
			header("Location: ".base_url('login'));
			# code...
		}
	}
	public function logmeout(){
		$this->session->unset_userdata('login');

		header("Location: ".base_url('login'));
	}
}