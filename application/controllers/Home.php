<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->Library('session');
		$this->load->model('user_model');
		if (empty($this->session->login)) {
			header("Location: ".base_url('login'));
			# code...
		}
	}


	public function index()

	{
		// $this->load->helper('date');
		// $waktu = date('Y-m-d');
		// echo formatHariTanggal($waktu);
		$this->load->model('user_model');
		$data=$this->user_model->get_last_ten_menus();
		$this->load->view('home',array('menu'=>$data
			));
		
	}
}
