<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Menu extends CI_Controller{
	function __construct(){
		parent::__construct(); 

		$this->load->model('user_model');
		$this->load->helper('login_helper');
		cekLogin();
		$this->load->library('datatables');
		
	}
	public function index(){
		
		$data=$this->user_model->get_last_ten_menus();
		$this->load->view('menu',array('menu'=>$data
			));
	}
	public function edit($id){
		$data=$this->user_model->get_last_ten_menus();
		$menu_data=$this->user_model->get_menu($id);
		
		if (count($menu_data)>0) {
			$this->load->view('crudmenu',array('menu'=>$data,
				'menu_data'=>$menu_data[0]
				));

	# code...
		}else{
			header("Location: ".base_url('menu'));
		}
		
	}
	public function add(){
		$data=$this->user_model->get_last_ten_menus();
		$this->load->view('crudmenu',array('menu'=>$data
			));
		
	}
	public function save(){
		$data = $this->input->post();
		$this->user_model->save($data); 
		header("Location: ".base_url('menu'));
	}
	public function delete($id){
		$this->user_model->delete($id);
		header("Location: ".base_url('menu'));
	}
	public function data(){
		$this->datatables->select('id, tittle, slug, icon, id as option');
		$this->datatables->from('menu');
		echo $this->datatables->generate();
	}

}