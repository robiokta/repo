 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class Siswa extends CI_Controller {

 	public function __construct(){
 		parent::__construct();

 		$this->load->model('SiswaModel');
 		$this->load->model('jabatan_model');
 	} 

 	public function index(){
 		$data['list']= $this->jabatan_model->list();
 		$this->load->view('view', $data);
 	}

 	public function export(){
		// Load plugin PHPExcel nya

 		$data = $this->jabatan_model->list();
 		$judul = "";
 		$this->load->library('Libexcel');
 		$this->libexcel->setActiveSheetIndex(0);
 		$this->libexcel->getActiveSheet()->setTitle('Pegawai');

 		$this->libexcel->setActiveSheetIndex(0)->mergeCells('A1:D1')->setCellValue('A1', 'ID');
 		$this->libexcel->getActiveSheet()->setCellValue('A2', 'Jabatan');

 		$i = 3;
 		foreach ($data as $tampil):
 			$this->libexcel->getActiveSheet()->setCellValue('A'.$i, $tampil->id_list_jabatan);
 		$this->libexcel->getActiveSheet()->setCellValue('B'.$i, $tampil->nama_jabatan);


 		$i++;
 		endforeach;
 		$filename='nama_file.xls'; 
    header('Content-Type: application/vnd.ms-libexcel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); 
    header('Cache-Control: max-age=0'); //no cache
    ob_end_clean();
    $objWriter = PHPExcel_IOFactory::createWriter($this->libexcel, 'Excel5');  
    $objWriter->save('php://output');
}
public function exportpdf()
{
	
	$data_user = array();
	$data_user['users'] = $this->SiswaModel->viewpegawai();
	$this->load->view('views',$data_user);
	$html = $this->output->get_output();
	$this->load->library('pdf');
	// $pdf = new Pdf();
// print_r($this->dompdf);
	$this->dompdf->loadHtml($html);
	 $this->dompdf->setPaper('A4', 'landscape');
$this->dompdf->render();
	 $this->dompdf->stream("welcome.pdf", array("Attachment"=>0));
	 
 } public function chart(){
	$data_user['users'] = $this->SiswaModel->chart();
	$this->load->view('chart',$data_user);

 }

}
